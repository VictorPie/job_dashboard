""" Pôle Emploi Robot """

import requests_html, re
from requests_html import HTMLSession

CodeRobot = "PoE_"
ThemeRecherche = "data"
RechercheMaitre = "https://candidat.pole-emploi.fr/offres/recherche?domaine=M18&motsCles="+ThemeRecherche+"&offresPartenaires=false&rayon=10&tri=1"
UrlOffre = "https://candidat.pole-emploi.fr/offres/recherche/detail/"

ListeRefOffres = set()

def InitSession():
    NbOffresSession = HTMLSession().get(RechercheMaitre).html.find("#zoneAfficherListeOffres > h1")[0].text
    NbOffresSession = int(re.match("\d+", NbOffresSession).group(0))

    if NbOffresSession == 0:
        print(f"Auccune offre pour cette recherche ({ThemeRecherche})")
    else:
        if NbOffresSession % 150 == 0:
            NbPagesSession = NbOffresSession // 150
        else:
            NbPagesSession = (NbOffresSession // 150) + 1

        print(NbOffresSession, NbPagesSession)
        Session(NbPagesSession)
        return NbPagesSession

def Session(NbPagesSession):
    for page in range(NbPagesSession + 1):
        PageOffre = page * 150
        #print(PageOffre)
        RechercheOffres = HTMLSession().get(RechercheMaitre + "&range=" + str(PageOffre - 150) + "-" + str(PageOffre))
        #print(RechercheMaitre + "&range=" + str(PageOffre - 149) + "-" + str(PageOffre))
        RefOffres = RechercheOffres.html.find("li.result")

        for ref in RefOffres:
            
            # if (Requette BdD verif presence CodeRobot+ref offres) 
                # pass
            # else:
            ListeRefOffres.add(ref.attrs["data-id-offre"])
    
    if len(ListeRefOffres) == 0:
        print("Aucune mise à jours nécessaire")
    print(len(ListeRefOffres))
    ScrapPageOffre()

def ScrapPageOffre():
    for ref in ListeRefOffres:
        lien_source = f"https://candidat.pole-emploi.fr/offres/recherche/detail/{ref}"
        test = HTMLSession().get(lien_source)
        
        ligne = {"PKID":CodeRobot+ref, "source":"Pôle-emploi", }
        try:  
            ligne["libellé"] = test.html.find(".t2.title", first = True).text
            ligne["nom_entreprise"] = test.html.find(".t4.title", first = True).text
            ligne["code_postal"] = test.html.find(".t4.title-complementary span span[content]")[0].attrs["content"]
            ligne["ville"] = test.html.find(".t4.title-complementary span span[content]")[1].attrs["content"]
        except AttributeError:
            continue
        print(ref)
        InsertionBdD(ligne)

def InsertionBdD(ligne):
    # Insert BdD
    print(ligne)



InitSession()