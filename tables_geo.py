import sqlalchemy as db
import pandas as pd

ville_file = "DocUtile/villes_france.csv"
dept_file = "DocUtile/departements-francais.csv"
engine = db.create_engine('postgresql://dashboard:job@localhost:5432/job_dashboard')

if not engine.dialect.has_table(engine, "villes"):
    with open(ville_file, 'r') as file:
        data_ville = pd.read_csv(file, low_memory=False)
        data_ville.to_sql('villes', con=engine, index=True, index_label='id', if_exists='append')
        print("table ville created")


with open(dept_file, 'r') as file:
    data_dept = pd.read_csv(file, sep='\t', low_memory=False)
    data_dept.to_sql('dept', con=engine, index=True, index_label='id', if_exists='replace')
    print("table dept created")
