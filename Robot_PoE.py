""" Pôle Emploi Robot """

""" Projet Job DashBoard : Groupe 2 
Module by Julien
Scrap by Victor & Julien
SQL by Julien """

import requests_html, re, DBProtocol, RobotsPostProcess, datetime
from requests_html import HTMLSession

source = "Pôle-emploi"
CodeRobot = "PoE_"
ThemesRecherches = ["data", "python"] # , "python"
RechercheMaitre = "https://candidat.pole-emploi.fr/offres/recherche?domaine=M18&offresPartenaires=true&rayon=10&tri=1"
UrlOffre = "https://candidat.pole-emploi.fr/offres/recherche/detail/"

Now = datetime.datetime.now()
ToDay = Now.date()
print(ToDay, type(ToDay))

compt_scraps = set()
compt_m_a_j = set()

ListeDepartements = DBProtocol.ReqSQL("""SELECT "NUMERO" FROM dept;""", None)
ListeTemp = []
for dept in ListeDepartements:
    ListeTemp.append(dept[0])
ListeDepartements = ListeTemp


def main():
    DBProtocol.ExeSQL(f"UPDATE TabOffres SET en_ligne = %s, hors_ligne = %s WHERE source = '{source}' AND hors_ligne IS NULL", (False, ToDay))
    DBProtocol.Commit()

    for theme in ThemesRecherches :
        InitSession(theme)

    print("###", len(compt_scraps), "nouvelle(s) offre(s) ajoutée(s)")
    print("###", len(compt_m_a_j), "offre(s) mise(s) à jour")

    TempsExe = datetime.datetime.now() - Now
    print("Temps d'exe :", TempsExe)


def InitSession(theme):
    Recherche = RechercheMaitre + "&motsCles=" + theme
    try :
        NbOffresSession = HTMLSession().get(Recherche).html.find("#zoneAfficherListeOffres > h1")[0].text
        NbOffresSession = int(re.match("\d+", NbOffresSession).group(0))
    except:
        NbOffresSession = 0

    if NbOffresSession == 0:
        print(f"Aucune offre pour cette recherche ({theme})")
    elif NbOffresSession > 1050:
        SessionDepartements(Recherche, theme)
    else:
        if NbOffresSession % 150 == 0:
            NbPagesSession = NbOffresSession // 150
        else:
            NbPagesSession = (NbOffresSession // 150) + 1

        #print("Nb Offres :", NbOffresSession, "Nb pages :", NbPagesSession)
        Session(NbOffresSession, NbPagesSession, Recherche)
        #return NbPagesSession


def SessionDepartements(recherche, theme):
    for dept in ListeDepartements:
        Recherche = recherche + "&lieux=" + dept + "D"
        try :
            NbOffresSession = HTMLSession().get(Recherche).html.find("#zoneAfficherListeOffres > h1")[0].text
            NbOffresSession = int(re.match("\d+", NbOffresSession).group(0))
        except:
            NbOffresSession = 0

        if NbOffresSession > 1050:
            print(f"Trop d'offres dans le département {dept} pour cette recherche ({theme})")
            print("Seul les 1050 premieres offres vont êtres recoltées")

        if NbOffresSession == 0:
            print(f"Aucune offre dans le département {dept} pour cette recherche ({theme})")
        else:
            if NbOffresSession % 150 == 0:
                NbPagesSession = NbOffresSession // 150
            else:
                NbPagesSession = (NbOffresSession // 150) + 1

            print("Département :", dept, "Nb Offres :", NbOffresSession, "Nb pages :", NbPagesSession)
            Session(NbOffresSession, NbPagesSession, Recherche)


def Session(NbOffresSession, NbPagesSession, Recherche):
    compt_sess_scraps = 0
    for p in range(NbPagesSession):
        #print("page :", p + 1)
        PageOffre = (p + 1) * 150
        if p == 0:
            UrlRecherche = Recherche + "&range=" + "0-150"
            RechercheOffres = HTMLSession().get(UrlRecherche)
        elif p == NbPagesSession - 1:
            UrlRecherche = Recherche + "&range=" + str(PageOffre - 149) + "-" + str(NbOffresSession + 1)
            RechercheOffres = HTMLSession().get(UrlRecherche)
        else:
            UrlRecherche = Recherche + "&range=" + str(PageOffre - 149) + "-" + str(PageOffre)
            RechercheOffres = HTMLSession().get(UrlRecherche)
        #print(UrlRecherche)
        RefOffres = RechercheOffres.html.find("li.result")

        for i, ref in enumerate(RefOffres):
            ref = ref.attrs["data-id-offre"]
            RefLigne = CodeRobot + ref
            lien_source = UrlOffre + ref
            Page = HTMLSession().get(lien_source)
            CompteurRef = i + 1 + p * 150

            try: # Procédure paliative au probleme de maintenance du site Pôle Emplois 
                date_publica = Page.html.find("span[content][itemprop='datePosted']")[0].attrs["content"]
            except:
                print(CompteurRef, CodeRobot + ref, ">> Impossible de procéder au scrap")
                continue
            
            date_publica = datetime.datetime.strptime(date_publica, "%Y-%m-%d").date()
            TabPKID = DBProtocol.ReqSQL("""SELECT PKID FROM TabOffres WHERE PKID = %s;""", (RefLigne,))
            
            if TabPKID == [] :
                compt_scraps.add(RefLigne)
                compt_sess_scraps += 1
                ScrapPageOffre(date_publica, ref, CompteurRef, NbOffresSession)
            elif DBProtocol.ReqSQL("""SELECT date_publica FROM TabOffres WHERE PKID = %s;""", (RefLigne,))[0][0] != date_publica :
                TabPublica = DBProtocol.ReqSQL("""SELECT date_publica FROM TabOffres WHERE PKID = %s;""", (RefLigne,))[0][0]
                DBProtocol.Del("TabOffres", "PKID", (RefLigne,))
                DBProtocol.Commit()
                ScrapPageOffre(date_publica, ref, CompteurRef, NbOffresSession)
                print(CompteurRef, "/", NbOffresSession, TabPKID[0][0], '>> Offre mise à jours', TabPublica, date_publica)
                compt_m_a_j.add(RefLigne)
                # WIP Assignement expression
            else:
                DBProtocol.ExeSQL(f"""UPDATE TabOffres SET en_ligne = %s, hors_ligne = %s WHERE PKID = '{RefLigne}';""", (True, None))
                DBProtocol.Commit()
                print(CompteurRef, "/", NbOffresSession, RefLigne, '>> Offre toujours en ligne')

    if compt_sess_scraps == 0:
        print("Aucune offre à ajouter")


def ScrapPageOffre(date_publica, ref, CompteurRef, NbOffresSession):
    print(CompteurRef, "/", NbOffresSession, CodeRobot + ref, ">> Scrap")
    lien_source = UrlOffre + ref
    Page = HTMLSession().get(lien_source)

    ligne = {"PKID":CodeRobot+ref, "lien_source":lien_source, "source":source, "date_publica":date_publica, "en_ligne":True, "hors_ligne":None}

    try:  
        ligne["libellé"] = Page.html.find(".t2.title", first = True).text
    except AttributeError:
        ligne["libellé"] = "Non Renseigné"
    
    try:    
        ligne["nom_entreprise"] = Page.html.find(".t4.title", first = True).text
    except AttributeError:
        ligne["nom_entreprise"] = "Non Renseigné"
    
    try:        
        ligne["code_postal"] = Page.html.find(".t4.title-complementary span span[content]")[0].attrs["content"]
    except (IndexError, AttributeError):
        ligne["code_postal"] = "Non Renseigné"
    
    try:
        ligne["ville"] = Page.html.find(".t4.title-complementary span span[content]")[1].attrs["content"]
    except (IndexError, AttributeError):
        ligne["ville"] = "Non Renseigné"

    try:    
        ligne["type_contrat"] = Page.html.find(".description-aside dd", first = True).text
    except AttributeError:
        ligne["type_contrat"] = "Non Renseigné"


    try:    
        ligne["corps"] = Page.html.find("div.description > p", first = True).text
    except AttributeError:
        ligne["corps"] = "Non Renseigné"
    
    ligne = RobotsPostProcess.Analyse(ligne)

    DBProtocol.InsertTabOffres(ligne)
    DBProtocol.Commit()