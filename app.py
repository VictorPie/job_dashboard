from flask import Flask, render_template, redirect, request, url_for
import DBProtocol, graph, DBQuery

app = Flask(__name__)

DBProtocol.ConnexionBD()

TotalOffres = DBQuery.count_all_offres()
TotalOffresEnLigne = DBQuery.count_all_offres_on_line()

@app.route("/", methods=["GET"])
def get_home():
    return render_template(
        "jdb_index.html",
        titre = """Protoype du Job DashBoard du Groupe 2 : Victor, Adrien, Hamid & Julien""",
        TotalOffres = TotalOffres,
        TotalOffresEnLigne = TotalOffresEnLigne,
        TotalOffresPython = DBProtocol.ReqSQL(f"select count(*) from TabOffres WHERE python IS TRUE", None)[0][0],
        TotalOffresSql = DBProtocol.ReqSQL(f"select count(*) from TabOffres WHERE sql IS TRUE", None)[0][0],
        TotalOffresData = DBProtocol.ReqSQL(f"select count(*) from TabOffres WHERE data IS TRUE", None)[0][0],
        TotalOffresPerti3 = DBProtocol.ReqSQL(f"select count(*) from TabOffres WHERE perti_auto = 3", None)[0][0],
        )


@app.route("/consult", methods=["GET"])
def get_consult():
    title_search = request.args.get("title_search", "")
    page = int(request.args.get("page", 1))
    limit = int(request.args.get("limit", 20))
    order_by = request.args.get("order_by", "libellé")
    sort = request.args.get("sort", "descending")
    search_where = request.args.get("search_where", "libellé")

    conn = DBProtocol.ConnexionBD()[0]
    offres = DBQuery.get_all_offres(
        title_search = title_search,
        search_where = search_where,
        page = page,
        limit = limit,
        order_by = order_by,
        sort = sort
        )
    nb_results = DBQuery.count_offres(title_search=title_search, search_where=search_where)
    num_offres = DBQuery.count_all_offres()

    prev_url = None
    if (page > 1):
        next_url_args = dict(request.args)
        next_url_args["page"] = page - 1
        prev_url = url_for("get_consult", **next_url_args)

    next_url = None
    if (page * limit) < num_offres and len(offres) == limit:
        next_url_args = dict(request.args)
        next_url_args["page"] = page + 1
        next_url = url_for("get_consult", **next_url_args)

    return render_template(
        "jdb_consult.html",
        titre = "Consultations des offres en BdD",
        TotalOffres = TotalOffres,
        TotalOffresEnLigne = TotalOffresEnLigne,
        offres = offres,
        nb_results = nb_results,
        num_offres = num_offres,
        title_search = title_search,
        search_where = search_where,
        limit = limit,
        order_by = order_by,
        sort = sort,
        prev_url = prev_url,
        next_url = next_url
        )


@app.route("/wip", methods=["GET"])
def get_wip():
    return render_template(
        "wip.html",
        titre = "Développement en cours",
        TotalOffres = TotalOffres,
        TotalOffresEnLigne = TotalOffresEnLigne,
        )


@app.route("/graph")
def get_graph():
    return render_template(
        "jdb_graph.html",
        titre = "Visualisation des statistiques",
        TotalOffres = TotalOffres,
        TotalOffresEnLigne = TotalOffresEnLigne,
        GraphA = graph.graph_regions(),
        GraphB = graph.CamTypeContrat(),
        GraphC = graph.CamTypeLangage(),
        GraphD = graph.HistLangages()
        )